const { run, series } = require('./utils/orchestrator');
const { startServer } = require('./server');
const { buildFiles } = require('./webpack');
const { cleanDir } = require('./clean');
const { mergeFolders } = require('./bundle');
const log = require('./utils/log');


const clean = cleanDir;
const serve = series(cleanDir, startServer);
const build = series(cleanDir, buildFiles, mergeFolders);

run({
    serve,
    build,
    clean,
}).catch(log.error);
