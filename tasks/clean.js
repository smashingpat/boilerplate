const rimraf = require('rimraf');
const { parallel } = require('./utils/orchestrator');


const cleanFolder = folderPath => new Promise((resolve) => {
    rimraf(folderPath, resolve);
});

const cleanBuild = config => cleanFolder(config.folders.build);
const cleanDist = config => cleanFolder(config.folders.dist);
const cleanDir = parallel(cleanBuild, cleanDist);

module.exports = {
    cleanDir,
};
