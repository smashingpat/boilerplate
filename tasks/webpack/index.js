const webpack = require('webpack');
const { createConfig } = require('./create-config');

const createCompiler = webpackConfig => webpack(webpackConfig);

const buildFiles = config => new Promise((resolve, reject) => {
    const webpackConfig = createConfig(config);
    const compiler = createCompiler(webpackConfig);

    compiler.run((err, stats) => {
        if (err) {
            reject(err);
        } else {
            console.log(stats.toString({
                colors: true,
                chunks: false,
            }));
            resolve();
        }
    });
});

function createConfigWithHMR(config) {
    return createConfig({
        ...config,
        hmrEnabled: true,
    })
}

module.exports = {
    createConfig,
    createConfigWithHMR,
    createCompiler,
    buildFiles,
};
