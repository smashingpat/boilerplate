const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


function createConfig(config) {
    const staticFolder = 'static/';
    const rootPath = path.resolve(__dirname, '../');
    const { hmrEnabled, mode } = config;
    const isProduction = mode === 'production';

    const createCssLoaders = ({ modules = false, sass = false } = {}) => [
        isProduction ? MiniCssExtractPlugin.loader : require.resolve('style-loader'),
        {
            loader: require.resolve('css-loader'),
            options: {
                sourceMap: true,
                minimize: true,
                modules,
                camelCase: 'only',
            },
        },
        {
            loader: require.resolve('postcss-loader'),
            options: {
                sourceMap: true,
            },
        },
        sass && {
            loader: require.resolve('fast-sass-loader'),
            options: {
                sourceMap: true,
                includePaths: [config.folders.source],
            },
        },
    ].filter(Boolean);

    return {
        // set the build mode
        mode,
        // use normal source-maps in production, else the cheaper alternative
        devtool: isProduction ? 'source-map' : 'cheap-module-source-map',
        optimization: {
            namedModules: true,
            splitChunks: {
                name: 'vendor',
                chunks: 'all',
            },
            noEmitOnErrors: true,
            concatenateModules: true,
        },
        // set the context of the config to the root
        context: rootPath,
        // set the entry files and babel-polyfill
        // if HMR is enabled insert the reloading/injection script needed
        entry: [
            hmrEnabled && 'webpack-hot-middleware/client?reload=true',
            `./${path.relative(rootPath, config.folders.source)}/main`,
        ].filter(Boolean),
        // set the output of the generated files
        output: {
            path: config.folders.build,
            filename: `${staticFolder}[name].bundle.js`,
            publicPath: config.publicPath,
        },
        // set how the modules are resolved when imported.
        // .jsx and .js are searched by default without specifying
        // a file extension on importing
        resolve: {
            extensions: ['.jsx', '.js'],
            alias: {
                source: config.folders.source,
            },
        },
        // set the module loaders by extension
        module: {
            rules: [
                {
                    test: /\.?worker\.js$/,
                    use: require.resolve('worker-loader'),
                },
                {
                    test: /\.jsx?$/,
                    use: [
                        require.resolve('thread-loader'),
                        {
                            loader: require.resolve('babel-loader'),
                            options: {
                                plugins: ['react-hot-loader/babel'],
                            },
                        },
                    ],
                    exclude: /node_modules/,
                },
                {
                    test: /\.scss$/,
                    exclude: /\.module\.scss$/,
                    use: createCssLoaders({
                        sass: true,
                    }),
                },
                {
                    test: /\.module\.scss$/,
                    use: createCssLoaders({
                        sass: true,
                        modules: true,
                    }),
                },
            ],
        },
        plugins: [
            hmrEnabled && new webpack.HotModuleReplacementPlugin(),
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify(process.env.NODE_ENV),
                    TARGET: JSON.stringify(config.target),
                },
            }),
            isProduction && new MiniCssExtractPlugin({
                filename: `${staticFolder}[name].css`,
                chunkFilename: `${staticFolder}[id].css`,
            }),
            new HtmlWebpackPlugin({
                template: path.resolve(config.folders.public, './index.html'),
                inject: true,
            }),
        ].filter(Boolean),
    };
}

module.exports = {
    createConfig,
};
