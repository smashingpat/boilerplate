const chalk = require('chalk');
const browserSync = require('browser-sync');
const compression = require('compression');
const historyApiFallback = require('connect-history-api-fallback');
const createWebpackDevMiddleware = require('webpack-dev-middleware');
const createWebpackHotMiddleware = require('webpack-hot-middleware');
const { createCompiler, createConfigWithHMR, createConfig } = require('../webpack');
const extraMiddleware = require('./extra-middleware');
const log = require('../utils/log');


function getCssAssets(stats) {
    const { assets } = stats.compilation;
    const emitted = Object.keys(assets)
        .filter(name => /.css$/.test(name) && assets[name].emitted);

    return emitted;
}

const hookDevServer = (config, browserSyncInstance, compiler) => {
    let isWatching = false;
    const tapConfig = { name: 'dev-server-hook' };
    const onCompile = () => {
        browserSyncInstance.notify('Rebuilding...');
    };
    const onDone = (stats) => {
        if (!isWatching) return;

        browserSyncInstance.reload(getCssAssets(stats));
        log.start('done compiling');
    };

    compiler.hooks.watchRun.tap(tapConfig, () => {
        isWatching = true;
    });
    compiler.hooks.compilation.tap(tapConfig, onCompile);
    compiler.hooks.done.tap(tapConfig, onDone);
};


const startServer = config => new Promise((resolve, reject) => {
    const server = browserSync.create();
    // disable HMR in production mode
    const hmrEnabled = !config.production;
    const webpackConfig = hmrEnabled
        ? createConfigWithHMR(config)
        : createConfig(config);
    const compiler = createCompiler(webpackConfig);

    hookDevServer(config, server, compiler);

    server.init({
        open: config.open,
        port: config.port,
        middleware: [
            compression(),
            extraMiddleware(config),
            historyApiFallback(),
            createWebpackDevMiddleware(compiler, {
                publicPath: '/',
                stats: 'errors-only',
                logger: {
                    trace: () => {},
                    debug: () => {},
                    info: () => {},
                    warn: () => {},
                    error: log.error,
                },
                logLevel: 'error',
            }),
            hmrEnabled && createWebpackHotMiddleware(compiler, {
                log: false,
            }),
        ].filter(Boolean),
        server: {
            baseDir: config.folders.public,
        },
        logLevel: false,
        logFileChanges: false,
        files: ['**/*.(html|css)'],
    }, (err, bs) => {
        if (err) reject(err);

        const configuredPort = config.port;
        const port = bs.options.get('port');
        let message = `is running at ${chalk.bold(`http://localhost:${port}`)}`;

        // if the port is not the same as the original config, give a heads up
        if (port !== configuredPort) {
            message += chalk.red(` (not ${config.port})`);
        }

        log.info(message);
        resolve();
    });
});

exports.startServer = startServer;
