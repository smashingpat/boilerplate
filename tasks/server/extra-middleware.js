// eslint-disable-next-line no-unused-vars
module.exports = function extraMiddleware(config) {
    return (request, response, next) => {
        // do something with the response, else call next
        next();
    };
};

