const chalk = require('chalk');
const path = require('path');
const log = require('./utils/log');
const { getLatestArgument } = require('./utils/cli-arguments');


const modes = {
    production: 'production',
    development: 'development',
    test: 'test',
};

const targets = {
    local: 'local',
    production: 'production',
};

const createConfig = () => {
    const config = {
        port: getLatestArgument('port', 3000),
        open: getLatestArgument('open', false),
        mode: getLatestArgument('mode', modes.development),
        target: getLatestArgument('target', targets.local),
        publicPath: '/',
        folders: {
            source: path.resolve(__dirname, '../source'),
            public: path.resolve(__dirname, '../public'),
            build: path.resolve(__dirname, '../_build'),
            dist: path.resolve(__dirname, '../_dist'),
        },
    };

    if (!Object.prototype.hasOwnProperty.call(modes, config.mode)) {
        throw new Error(`wrong mode \`${config.mode}\` selected in the command, should be one: \`${Object.keys(modes).join(', ')}\``);
    }

    if (!Object.prototype.hasOwnProperty.call(targets, config.target)) {
        throw new Error(`wrong mode \`${config.target}\` selected in the command, should be one: \`${Object.keys(targets).join(', ')}\``);
    }

    // set the NODE_ENV to production or development
    process.env.NODE_ENV = config.mode;

    ['mode', 'target'].forEach((key) => {
        log.info(`${key} is set to ${chalk.bold.blue(config[key])}`);
    });

    return config;
};

module.exports = createConfig;
