const log = require('./log');
const configureConfig = require('../config');
const { hasArgument, getLatestArgument } = require('./cli-arguments');

async function triggerTask(task, args = []) {
    const { name } = task;
    log.start(`${name} building`);

    const result = await task(...args);
    log.success(`${name} success`);

    return result;
}

const createParallel = (...tasks) => async function parallel(...taskArguments) {
    await Promise.all(tasks.map(task => triggerTask(task, taskArguments)));
};

const createSeries = (...tasks) => async function series(...taskArguments) {
    for (const task of tasks) {
        await triggerTask(task, taskArguments);
    }
};


const createRunner = async (tasks = {}) => {
    const taskNames = Object.keys(tasks);
    let taskName = getLatestArgument('task');
    const task = tasks[taskName];

    if (task) {
        const config = await configureConfig();
        await task(config);
    } else {
        log.error(new Error(`task \`${taskName}\` found, try ${taskNames.join(', ')}`));
    }
};

module.exports = {
    run: createRunner,
    parallel: createParallel,
    series: createSeries,
};
