const { argv } = require('yargs');

const hasArgument = key => Object.prototype.hasOwnProperty.call(argv, key);
const getLatestArgument = (key, fallback) => {
    if (hasArgument(key)) {
        if (Array.isArray(argv[key])) {
            return argv[key][argv[key].length - 1];
        }
        return argv[key];
    }
    return typeof fallback === 'function' ? fallback() : fallback;
};

module.exports = {
    hasArgument,
    getLatestArgument,
};
