const chalk = require('chalk');

function createTimestamp(date) {
    return [
        String(date.getHours()).padStart(2, '0'),
        String(date.getMinutes()).padStart(2, '0'),
        String(date.getSeconds()).padStart(2, '0'),
    ].join(':');
}

function createLogObj(opts) {
    let logObj = { date: new Date() };
    if (typeof opts === 'string') {
        logObj.message = opts;
    } else if (opts.stack) {
        const [message, ...stack] = opts.stack.split('\n');
        logObj.message = message;
        logObj.additional = stack.map(s => s.trim()).join('\n');
    } else {
        logObj = Object.assign(logObj, opts);
    }
    return logObj;
}

function createLoggers(logTypes) {
    const longestName = logTypes.reduce((c, o) => (o.name.length > c ? o.name.length : c), 0);
    return logTypes.reduce((acc, logType) => ({
        ...acc,
        [logType.name]: function log(message) {
            const logObj = createLogObj(message);
            process.stdout.write([
                `[${chalk.dim(createTimestamp(logObj.date))}]`,
                `${chalk[logType.color](logType.name.padEnd(longestName))}`,
                logObj.message,
            ].join(' '));
            if (logObj.additional) {
                process.stdout.write('\n');
                process.stdout.write(chalk.dim(logObj.additional.split('\n').map(s => `    ${s}`).join('\n')));
            }
            process.stdout.write('\n');
        },
    }), {});
}

const logTypes = [
    { name: 'info', color: 'yellow' },
    { name: 'start', color: 'blue' },
    { name: 'success', color: 'green' },
    { name: 'error', color: 'red' },
];

module.exports = createLoggers(logTypes);
