import React from 'react';
import ReactDOM from 'react-dom';
import Root from './view/root';
import './styles/global.scss';


const mountElement = document.getElementById('mount');

function renderReact(RootComponent) {
    ReactDOM.render(
        <RootComponent />,
        mountElement,
    );
}

renderReact(Root);
