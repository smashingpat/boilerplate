module.exports = {
    env: {
        "browser": true,
        "node": true,
        "jest/globals": true,
    },
    extends: "airbnb",
    parser: "babel-eslint",
    plugins: [
        "jest",
    ],
    rules: {
        "indent": ["error", 4],
        "react/jsx-indent": ["error", 4],
        "react/jsx-indent-props": ["error", 4],
        "react/jsx-filename-extension": "off",
        "import/no-extraneous-dependencies": ["error", {
            "devDependencies": true,
            "optionalDependencies": false,
            "peerDependencies": false,
        }],
        "import/prefer-default-export": "off",
    },
};
