# Boilerplate

Compact Boilerplate powered by [webpack][webpack] and [browser-sync][browser-sync].

Requires nodejs v8.0.0 & npm v5.1.0.

## Starting up

```sh
npm install
npm start
```

## Tasks

```sh
npm run -
        start  # gives you a list of choices what to run
        serve  # starts up a dev-server
        build  # builds the source files into _dist
        test   # runs the unit tests with jest
```

Also flags are supported to slightly change how the builds will work
```sh
# sets the build mode to production, you can also choose development
--mode=production
# sets the target to local, accessed through `process.env.TARGET`
--target=local
# opens the current devServer in the browser. Only works with the `serve` task
--open
# sets the port to a custom number. Only works with the `serve` task
--port=3000
```

[webpack]: https://webpack.js.org/
[browser-sync]: https://www.browsersync.io/

## import resolves

all imports made by the extensions `.js`, `.jsx`, `.scss`, `.css` are resolved to the `./source` folder. So you can import everything both relative to the file location and the source directory. Example:

`.js`, `.jsx`

```javascript
// from `./node_modules`
import { merge } from 'lodash'
// alias source from `./source`
import { Button } from 'source/view/components'
// relative from file
import something from '../../something'
```

`.scss`, `.css`

```scss
// with ~ from `./node_modules/normalize.css/normalize.css`
@import '~normalize.css/normalize.css';
// resolves in order from relative to `./source` to error
@import 'styles/variables';
```


# Sass & CSS

`.scss` files are by default extracted to a seperated `style.css` file, and can be made modular by adding `module` before the extension like: `my-awesome-component.module.scss`. This will automatically create an object when the file is imported with all the classes transformed to camelCase

```scss
// styles.module.scss
.button {
    background: tomato;

    &.is-active {
        background: red;
    }
}
```
```js
import styles from './styles.module.scss';
// styles =
// {
//     button: '[unique-button-name]',
//     isActive: '[unique-isactive-name]',
// }
```
